class AddUdidToDeviceToken < ActiveRecord::Migration
  def change
    add_column :device_tokens, :udid, :string
  end
end
