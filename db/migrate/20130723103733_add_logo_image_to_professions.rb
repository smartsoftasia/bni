class AddLogoImageToProfessions < ActiveRecord::Migration
  def self.up
    add_attachment :professions, :logo
    add_attachment :professions, :image
  end

  def self.down
    remove_attachment :professions, :logo
    remove_attachment :professions, :image
  end
end
