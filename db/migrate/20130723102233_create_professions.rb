class CreateProfessions < ActiveRecord::Migration
  def change
    create_table :professions do |t|
      t.string :title
      t.text :content
      t.string :phone_number
      t.string :email
      t.text :address
      t.string :name
      t.string :website
      t.timestamps
    end
  end
end
