class AddLogoUrlAndImageUrlToProfession < ActiveRecord::Migration
  def change
    add_column :professions, :logo_url, :string
    add_column :professions, :image_url, :string
  end
end
