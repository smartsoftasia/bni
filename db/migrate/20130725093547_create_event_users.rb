class CreateEventUsers < ActiveRecord::Migration
  def change
    create_table :event_users do |t|
      t.integer :event_id
      t.integer :user_id
      t.datetime :check_in_time
      t.integer :subtitute_of

      t.timestamps
    end
  end
end
