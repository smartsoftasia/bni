xml.instruct! :xml, version: "1.0" 
xml.rss version: "2.0" do
  xml.channel do
    xml.title "BNI RSS"
    xml.description "List of Feeds"
    xml.link "https://www.google.co.th"


    @feeds.each do |feed|
      xml.item do
        xml.title feed.title
        xml.description feed.content
        xml.pubDate feed.updated_at.to_s(:rfc822)
        xml.link "https://www.google.co.th"
      end
    end
  end
end

