atom_feed do |rss|
  rss.title "BNI RSS"
  rss.updated @feeds.first.updated_at

  @feeds.each do |feed|
    rss.entry(feed, url: "") do |entry|
      entry.title feed.title
      entry.content feed.content
    end
  end

end