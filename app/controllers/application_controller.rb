class ApplicationController < ActionController::Base
  protect_from_forgery
  
  def notify(msg)
    require 'rubygems'
    require 'pushmeup'


    devices = DeviceToken.all

    DeviceToken.where(platform: "ios").each do |device|

        APNS.host = 'gateway.sandbox.push.apple.com' 
        APNS.port = 2195 
        APNS.pem  = "#{Rails.root}/cert/ck.pem"
        APNS.pass = 'bnismartsoftasia123'
        device_token = device.token
        APNS.send_notification(device_token, :alert => msg, :badge => 1, :sound => 'beep.wav')

    end

    destination = DeviceToken.where(platform: "android").map(&:token)

    GCM.host = 'https://android.googleapis.com/gcm/send'
    GCM.format = :json
    GCM.key = "AIzaSyCxqsMykV0khH8b46QKhtNzCvrz6gFzKFc" 
    # this is the key of google my API. It's will never change exept on explicit request.
    data = {:title => "BNI", :message => msg, :msgcnt => "1"} 
    # can be what I want, it's just an Json file
    GCM.send_notification( destination, data)

  end
end
