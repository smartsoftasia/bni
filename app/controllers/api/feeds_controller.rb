class Api::FeedsController < Api::BaseController

  def index
    @feeds = Feed.order('updated_at DESC')
  end
end

