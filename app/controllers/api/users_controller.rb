class Api::UsersController < Api::BaseController
  before_filter :check_token, :only => [:update]

  def create
    user = User.create(params[:user])
    if user.save
      render :status => 200, nothing: true
    else
      render :status => 400, nothing: true
    end
  end

  def update
    user = User.find_by_id(params[:id])
    if user = user.update_attributes(params[:user])
      render :status => 200, nothing: true
    else
      render :status => 400, nothing: true
    end
  end

  def authenticate
   if user = User.find_for_database_authentication(:email => params[:email])
      if user.valid_password?(params[:password])

        o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
        string  =  (0...10).map{ o[rand(o.length)] }.join

        user.access_token = string
        user.save
        render :json => user
      else
        render :status => 400, nothing: true
      end
    else
      render :status => 400, nothing: true
    end
  end

  def logout
    user = User.find_by_access_token(params[:access_token])
    user.access_token = nil
    if user.save
      render :status => 200, nothing: true
    end
  end

  def push_notification_all
    require 'rubygems'
    require 'pushmeup'


    devices = DeviceToken.all

    DeviceToken.where(platform: "ios").each do |device|

        APNS.host = 'gateway.sandbox.push.apple.com' 
        APNS.port = 2195 
        APNS.pem  = "#{Rails.root}/cert/ck.pem"
        APNS.pass = 'bnismartsoftasia123'
        device_token = device.token
        APNS.send_notification(device_token, :alert => 'Rails', :badge => 1, :sound => 'beep.wav')

    end

    destination = DeviceToken.where(platform: "android").map(&:token)

    GCM.host = 'https://android.googleapis.com/gcm/send'
    GCM.format = :json
    GCM.key = "AIzaSyCxqsMykV0khH8b46QKhtNzCvrz6gFzKFc" 
    # this is the key of google my API. It's will never change exept on explicit request.
    data = {:title => "BNI", :message => "Rails", :msgcnt => "1"} 
    # can be what I want, it's just an Json file
    GCM.send_notification( destination, data)

    render :status => 200, :nothing => true
  end

end