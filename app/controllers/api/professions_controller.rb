class Api::ProfessionsController < Api::BaseController
	def index
    @search = Profession.search(params[:q])
    @professions = @search.page(params[:page]).per(params[:per_page])

	  render :json => @professions
	end

	def show
    @profession = Profession.find(params[:id])
    render :json => @profession
	end

  def last_updated_at
    rss = Rss.domain
    render :json => {last_updated_at: Profession.last_updated_at, count: Profession.all.count, rss_url: rss}
  end
end
