class Api::EventsController < Api::BaseController

  def index
    events = Event.where("start_time >= ?", Time.now)
    render  :json => events
  end

  def show
    event = Event.find_by_id(params[:id])
    if event
      render :json => event
    end
  end

  def next_event
    event = Event.order("start_time asc").where("start_time > ?",Time.now).limit(1)
    render :json => event
  end

  def current_event
    events = Event.where("start_time < ? and end_time > ?", Time.now, Time.now)
    if !events.empty?
      render :json => events
    else
      render :status => 400, :nothing => true
    end
  end
end

