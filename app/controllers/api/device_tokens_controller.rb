class Api::DeviceTokensController < Api::BaseController
  def create
    device = DeviceToken.find_or_initialize_by_udid(params[:device][:udid])
    if device.update_attributes(params[:device])
      render :status => 200, :nothing => true
    end
  end
end

