class Api::BaseController < ApplicationController
  protect_from_forgery
  # before_filter :check_token, :only => :update

  def check_token
    if params[:access_token]
      if User.find_by_access_token(params[:access_token])
      	
      else
        render :status => 400, nothing: true
      end
    else
      render :status => 400, nothing: true
    end 	
  end   
end