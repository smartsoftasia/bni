class Api::EventUsersController < Api::BaseController
  before_filter :check_token, :only => [:create, :check_in]

  # def index
  #   events = Array.new

  #   meeting_list = EventUser.where(:user_id => params[:user_id]).map{|x| x.event}
  #   events << {:meeting_list => meeting_list}

  #   next_meeting_availlable = meeting_list.map{|x| x if ((x.start_time - Time.now)/ 1.hour).round == 1 or ((Time.now - x.end_time)/ 1.hour).round == 1}
  #   events << {:next_meeting_availlable => next_meeting_availlable}

  #   render :json => events
  # end

  def create
    event = Event.find_by_id(params[:event_user][:event_id])
    if event.code == params[:event][:code]
      event_user = EventUser.find_by_event_id_and_user_id(params[:event_user][:event_id], params[:event_user][:user_id])
      if event_user
          render :status => 409, nothing: true
      else
        event_user = EventUser.create(params[:event_user])
        event_user.check_in_time = Time.now.utc
        if event_user.save
          render :status => 200, nothing: true
        else
          render :status => 400, nothing: true
        end
      end
    else
      render :status => 400, nothing: true
    end
  end

  # def update
  #   event = Event.find_by_id(params[:event_user][:event_id])
  #   if event.code == params[:event][:code]
  #     event_user = EventUser.find_by_event_id_and_user_id(params[:event_user][:event_id], params[:event_user][:user_id])
  #     if event_user.check_in_time
  #       render :status => 400, nothing: true
  #     else
  #       if params[:check_in]
  #         event_user.check_in_time = Time.now.utc
  #         event_user.save
  #         return render :status => 200, nothing: true
  #       end

  #       if event_user.update_attributes(params[:event_user])
  #         render :status => 200, nothing: true
  #       else
  #         render :status => 400, nothing: true
  #       end
  #     end
  #   else
  #     render :status => 400, nothing: true
  #   end
  # end

  def check_in
    event = Event.find_by_id(params[:event_user][:event_id])
    if event.code == params[:event][:code]
      event_user = EventUser.find_by_event_id_and_user_id(params[:event_user][:event_id], params[:event_user][:user_id])
      if event_user.check_in_time
        render :status => 409, nothing: true
      else
        event_user.update_attributes(check_in_time: Time.now.utc)
        render :status => 200, nothing: true
      end
    else
      render :status => 400, nothing: true
    end    
  end

end

