ActiveAdmin.register_page "Notifications" do
  content :title => "Notifications" do
    div do
      render 'form'
    end
  end

  page_action :create , :method => :post do
    ApplicationController.new.notify(params[:message])
    flash[:notice] = "Settings are successfully sent!"
    redirect_to admin_notifications_url
  end

end