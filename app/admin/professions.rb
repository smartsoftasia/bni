ActiveAdmin.register Profession do
    form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Profession Details" do

        f.input :title
        f.input :content
        f.input :phone_number
        f.input :email
        f.input :address
        f.input :name
        f.input :website
        
             
        f.input :logo, :as => :file, :hint => f.template.image_tag(f.object.logo.url(:medium))
        f.input :image, :as => :file, :hint => f.template.image_tag(f.object.image.url(:medium))
      
    end    
    f.buttons
  end
end
