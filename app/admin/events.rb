ActiveAdmin.register Event do

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Event" do 
    f.input :name
    f.input :code 
    f.input :detail
    f.input :start_time, :as => :datetime_picker
    f.input :end_time, :as => :datetime_picker
    end
    f.buttons
  end

  show do |event|
    attributes_table do
      row :name
      row :detail
      row :start_time
      row :end_time
      row :code
    end

      table_for assigns[:event].event_users do
      column :id
      column "First Name", :user_id do |user|
        simple_format user.user.first_name 
      end
      column "Last Name", :user_id do |user|
        simple_format user.user.last_name 
      end
      column "Company Name" ,:user_id do |user|
        simple_format user.user.company_name 
      end
      column "Email", :user_id do |user|
        simple_format user.user.email 
      end
      column :check_in_time
      column "Substitute of", :subtitute_of do |substitute|
        simple_format "#{substitute.user.first_name} #{substitute.user.last_name}"
      end
    end
  end
end


