ActiveAdmin.register User do     
  index do                            
    column :email                     
    column :current_sign_in_at        
    column :last_sign_in_at           
    column :sign_in_count             
    default_actions                   
  end                                 

  filter :email                       

  form do |f|                         
    f.inputs "Personal Details" do       
      f.input :first_name                  
      f.input :last_name              
      f.input :company_name
      f.input :is_guest
      f.input :is_admin  
    end

    f.inputs "Events" do
      f.has_many :event_users do |p|
        p.input :event
        p.input :subtitute_of, :label => "Substitute", :as => :select, :collection => User.all.map{|u| ["#{u.last_name}, #{u.first_name}", u.id]}
      end
    end

    f.inputs "Account Details" do       
      f.input :email               
      f.input :password         
    end                                   
    f.actions                         
  end                                 
end                                   
