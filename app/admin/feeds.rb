ActiveAdmin.register Feed do
  form do |f|
    f.inputs "Feed info" do
      f.input :title
      f.input :content, :as => :rich
    end
    f.buttons
  end
end
