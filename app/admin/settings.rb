ActiveAdmin.register_page "Settings" do
  content :title => "Settings" do
    div do
      render 'form'
    end
  end

  page_action :update, :method => :post do
    params[:settings].each do |key, value|
      Rss.send("#{key}=", value)
    end
    flash[:notice] = "Settings are successfully updated!"
    redirect_to admin_settings_url
  end 
end