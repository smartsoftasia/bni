class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable
  # attr_accessible :title, :body
  attr_accessible :email, :password, :password_confirmation, :first_name, :last_name, :company_name, :is_admin, :is_guest, :access_token, :event_users_attributes

  has_many :event_users, :dependent => :destroy
  accepts_nested_attributes_for :event_users, :allow_destroy => true

  def user_type
    if is_guest == true
      1
    else
      0
    end
  end
end

