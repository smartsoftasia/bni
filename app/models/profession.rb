class Profession < ActiveRecord::Base
  attr_accessible :logo, :image, :address, :content, :email, :image, :logo, :name, :phone_number, :title, :website
  has_attached_file :logo, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"

  def self.last_updated_at
    profession = Profession.order("updated_at DESC").first
    if profession
      profession.updated_at.strftime("%d/%m/%Y")
    else
      ""
    end
  end

  def logo_url
    self.logo
  end

  def image_url
    self.image
  end
  
end
