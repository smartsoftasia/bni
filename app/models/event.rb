class Event < ActiveRecord::Base
  attr_accessible :detail, :end_time, :name, :start_time, :code
  has_many :event_users
end
