class EventUser < ActiveRecord::Base
  attr_accessible :check_in_time, :event_id, :subtitute_of, :user_id
  belongs_to :user
  belongs_to :event
end
