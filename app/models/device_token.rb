class DeviceToken < ActiveRecord::Base
  attr_accessible :token, :platform, :udid, :user_id
end
